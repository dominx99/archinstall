# Installation guide

## How to flash USB Drive

Example using `dd`
```
dd bs=4M if=/path/to/archlinux-version-x86_64.iso of=/dev/sda conv=fsync oflag=direct status=progress
```

## Install process

Connect to the internet via
[iwctl](https://wiki.archlinux.org/title/Iwd#iwctl)
```sh
iwctl
iwctl station wlan0 connect "Yerba Mate"
```

```sh
ls /sys/firmware/efi/efivars
timedatectl set-ntp true
```

```sh
cfdisk /dev/sdX
```

### Not encrypted disk
| Device | Name | Memory | Location | Filesystem |
|--------|------|--------|----------|------------|
| `/dev/sda1` | EFI | 550MB (min. 300MB) | /efi | EFI System |
| `/dev/sda2` | SWAP | 2x RAM | | SWAP |
| `/dev/sda3` | Root partition | Rest of memory | / | Linux filesystem |
| `/dev/sdb1` | Second root partition | Rest of memory | /workspace | Linux filesystem |

### Encrypted disk
| Device | Name | Memory | Location | Filesystem |
|--------|------|--------|----------|------------|
| `/dev/sda1` | EFI | 550M (min. 300M) | /efi | EFI System |
| `/dev/sda2` | Just in case | 1M | | Boot system |
| `/dev/sda3` | Root partition | Rest of memory | / | Linux filesystem |
| `/dev/sdb1` | Second root partition | Rest of memory | /workspace | Linux filesystem |


Check disk
```
fdisk -l
lsblk
```

Encrypt disk via [cryptsetup](https://gist.github.com/huntrar/e42aee630bee3295b2c671d098c81268)

For disk encryption check if dm_crypt is loaded
```sh
lsmod | grep dm_crypt
modprobe dm_crypt
```

### Decryption if needed
> takes about 30 minutes
```
cryptsetup reencrypt --decrypt <device_path>
```

```sh
cryptsetup luksFormat --type luks1 --use-random -S 1 -s 512 -h sha512 -i 5000 /dev/nvme0n1p3
cryptsetup open /dev/nvme0n1p3 cryptlvm
pvcreate /dev/mapper/cryptlvm
vgcreate vg /dev/mapper/cryptlvm

lvcreate -L 8G vg -n swap
lvcreate -L 50%FREE vg -n root
lvcreate -l 50%FREE vg -n home
```

### Not encrypted disk
```sh
mkfs.ext4 /dev/sda3 #root
mkswap /dev/sda2 #swap

mount /dev/sda3 /mnt (Root partition)
mount --mkdirr /dev/vg/home /mnt/home
swapon /dev/sda2
```

### Encrypted disk
```sh
mkfs.ext4 /dev/vg/root
mkfs.ext4 /dev/vg/home
mkswap /dev/vg/swap

mount /dev/vg/root /mnt
mount --mkdir /dev/vg/home /mnt/home
swapon /dev/vg/swap
```

---

```
mkfs.fat -F 32 /dev/sda1 #EFI
mount --mkdir /dev/sda1 /mnt/efi
```

```
pacstrap /mnt base base-devel linux linux-firmware mkinitcpio lvm2 vi dhcpcd wpa_supplicant
genfstab -U -p /mnt >> /mnt/etc/fstab
cat /mnt/etc/fstab
arch-chroot /mnt
ln -sf /usr/share/zoneinfo/Europe/Warsaw /etc/localtime
hwclock --systohc
vi /etc/locale.gen
```

```
en_US.UTF-8 UTF-8
pl_PL.UTF-8 UTF-8
```

```
locale-gen
```

```
vi /etc/locale.conf

LANG=pl_PL.UTF-8
LC_MESSAGES=en_US.UTF-8
```

```
vi /etc/vconsole.conf

KEYMAP=pl
FONT_MAP=8859-2
```

```
vi /etc/hostname
---
Thinkpad
```

```
vi /etc/hosts `optional`
---
127.0.0.1       localhost.localdomain               localhost
::1             localhost.localdomain               localhost
127.0.1.1       domin.localdomain	               domin
```

### Initramfs
Add the keyboard, encrypt, and lvm2 hooks to /etc/mkinitcpio.conf\
Note: ordering matters.

```
HOOKS=(base udev autodetect keyboard modconf block encrypt lvm2 filesystems fsck)
```

```sh
./install-scripts/automate-mkinitcpio
mkinitcpio -p linux
passwd

pacman -S grub efibootmgr os-prober intel-ucode dhcpcd wpa_supplicant dialog wget vim libnewt netctl ppp git iw wireless_tools openssh go
```

if you encrypted disk you have to update grub
```sh
blkid | grep LUKS | cut -d '"' -f 2
./install-scripts/automate-grub
cat /etc/default/grub
```

Install grub
```sh
grub-install --target=x86_64-efi  --bootloader-id=grub_uefi --efi-directory=/efi
grub-mkconfig -o /boot/grub/grub.cfg
```

Add secret key for disk encryption to not insert password twice
```sh
mkdir /root/secrets && chmod 700 /root/secrets
head -c 64 /dev/urandom > /root/secrets/crypto_keyfile.bin && chmod 600 /root/secrets/crypto_keyfile.bin
cryptsetup -v luksAddKey -i 1 /dev/nvme0n1p3 /root/secrets/crypto_keyfile.bin
mkinitcpio -p linux
```

Reboot
```sh
exit
umount -R /mnt
reboot
```

## Using makefile

Connect to the internet via
[iwctl](https://wiki.archlinux.org/title/Iwd#iwctl)
```sh
iwctl
station wlan0 connect "Yerba Mate"
exit
```

```
ls /sys/firmware/efi/efivars

./automate-init.sh
cfdisk /dev/sdX
./automate-encrypt-disk /dev/sda3 16G # <disk> <swap_size>
```

### Not encrypted disk
```sh
mkfs.ext4 /dev/sda3 #root
mkswap /dev/sda2 #swap

mount /dev/sda3 /mnt (Root partition)
swapon /dev/sda2
```

### Encrypted disk
```sh
mkfs.ext4 /dev/vg/root
mkfs.ext4 /dev/vg/home
mkswap /dev/vg/swap

mount /dev/vg/root /mnt
mount --mkdir /dev/vg/home /mnt/home
swapon /dev/vg/swap
```

---

```sh
mkfs.fat -F 32 /dev/sda1 #EFI
mount --mkdir /dev/sda1 /mnt/efi

./automate-pacstrap
./automate-genfstab

arch-chroot /mnt

make do-chrooted-stuff
./automate-mkinitcpio.sh # only if disk is encrypted
make do-mkinitpcio
make do-encrypted-grub-stuff # only if disk is encrypted
make do-install-grub
make do-add-crypto-key # only if disk is encrypted
```

```sh
exit
umount -R /mnt
reboot
```

## Post install

### LARBS

> Copy .ssh keys from pendrive to `/root/.ssh/id_rsa`\
chmod 600 on pub and private key
chmod 700 on .ssh folder

log in to root and clone repository with script and run the script
```sh
wifi-menu
git clone git@bitbucket.org:dominx99/archlinux.git
archlinux/post.sh
```
if you have problems with wifi-menu, you can change dhcp to static connection, where {name} is profile created while connecting
```sh
vi /etc/netctl/{name}
```
Sample config, but you should also check `/etc/netcl/examples`
```
IP=static
Address='192.168.0.123/24'
Gateway='192.168.0.1'
DNS=('192.168.0.1')
```
after script just reboot

```sh
git clone https://github.com/tarjoilija/zgen.git /home/domin/.zgen
chown -R domin:wheel /home/domin/.zgen
pacman -S fish
sudo su domin
chsh -s $(which zsh)
reboot
```

Install oh-my-fish! Go to github.com page
Install xflux - should be installed

```sh
yay -S brave-bin
```

```sh
yay -S slack-desktop
```

```sh
sudo npm install -g intelephense
```

-----
Issues with polybar
```sh
pip uninstall pygmnets
pacman -S polybar
```
Issues with docker-compose
```sh
pip uninstall click
pacman -S docker-compose
```

Use network manager
```
nmcli
```

Pass
```
gpg --import secret.gpg
pass init <gpg-id>
pass git init
pass git remote add origin <url>
pass git config pull.rebase true
pass git pull
```

Install alacritty-ligatures-git from yay


### Chezmoi
```
#root
./install-scripts/automate-adduser.sh
pacman -S chezmoi
sudo su domin

#domin
chezmoi init dominx99
# /home/domin/.config/chezmoi/chezmoi.yaml needs to be loaded first
chezmoi apply /home/domin/.config
chezmoi apply
exit

#just in case
sudo -u "$name" git clone https://github.com/tarjoilija/zgen.git "/home/$name/.zgen"
```
