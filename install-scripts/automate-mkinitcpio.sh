#!/usr/bin/env bash
cp /etc/mkinitcpio.conf /etc/mkinitcpio.conf.bck
sed -i "s|^HOOKS=.*|HOOKS=(base udev autodetect keyboard modconf block encrypt lvm2 filesystems fsck)|g" /etc/mkinitcpio.conf
