#!/usr/bin/env bash

timedatectl set-ntp true

if ! lsmod | grep -q dm_crypt; then
    echo "dm_crypt is not loaded, loading it now..."
    modprobe dm_crypt
fi
