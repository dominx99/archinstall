#!/usr/bin/env bash

# if $1 or $2 does not exist then exit
if [ -z "$1" ] || [ -z "$2" ]; then
    echo "Usage: $1 <disk> <swap_size>"
    exit 1
fi

cryptsetup luksFormat --type luks1 --use-random -S 1 -s 512 -h sha512 -i 5000 $1
cryptsetup open $1 cryptlvm
pvcreate /dev/mapper/cryptlvm
vgcreate vg /dev/mapper/cryptlvm

lvcreate -L $2 vg -n swap
lvcreate -L 50%FREE vg -n root
lvcreate -l 50%FREE vg -n home
