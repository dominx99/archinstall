#!/usr/bin/env bash

# if $1 is not present then exit
if [ -z "$1" ]; then
    echo "Usage: $1 <hostname>"
    exit 1
fi

ln -sf /usr/share/zoneinfo/Europe/Warsaw /etc/localtime
hwclock --systohc

# Uncomment en_US.UTF-8 UTF-8 and other needed localizations in /etc/locale.gen
sed -i 's/#pl_PL.UTF-8 UTF-8/pl_PL.UTF-8 UTF-8/' /etc/locale.gen
sed -i 's/#en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/' /etc/locale.gen

locale-gen

cat > /etc/locale.conf << EOF
LANG=pl_PL.UTF-8
LC_MESSAGES=en_US.UTF-8
EOF

cat > /etc/vconsole.conf << EOF
KEYMAP=pl
FONT_MAP=8859-2
EOF

echo "$1" > /etc/hostname

cat > /etc/hosts << EOF
127.0.0.1       localhost.localdomain               localhost
::1             localhost.localdomain               localhost
127.0.1.1       domin.localdomain	               domin
EOF

