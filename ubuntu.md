### Chezmoi

Run from branch:
```bash
snap install chezmoi --classic
sudo apt install git fish curl

rm ~/.ssh/know_hosts
echo "domin" > /tmp/username
chezmoi init --branch ubuntu-bspwm git@github.com:dominx99/dotfiles.git
```

Resets state of chezmoi:
```bash
chezmoi reset state
```
